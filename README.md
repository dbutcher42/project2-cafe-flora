# Project Cafe Flora Back End

This is a restaurant online ordering system. It allows a user to sign up for an account or recover a password before logging in. Once logged in users can add/remove items from the menu to the cart. From the cart the user can go to checkout where they can enter a coupon for a discount, set a tip, select pickup or delivery and then continue on to the payment page. On the payment page they enter their credit card information and submit the order.

**Features**

- Login functionality
- Registration for new customers.
- Password recovery via email.
- Customers can go to the menu and select items they want to purchase
- Customers can go to the cart and remove items, increase quantity, clear their cart completely, or go to checkout
- In checkout customers can add coupon codes for discounts, indicate pickup or delivery, enter a tip for delivery orders, and continue to payment
- In payment customers can enter their credit card information or use saved credit card information, then proceed with payment

**Technologies Used**
- Java 8
- HTML/CSS
- TypeScript
- Spring Boot
- JavaMail API
- JDBC
- SQL
- RDS
- Maven
- Angular
- Selenium
- Cucumber
- JUnit/Mockito
- AWS S3
- AWS EC2
- Jenkins
- Jasmine/Karma
- Git

**Front End Repository**
- https://gitlab.com/dbutcher42/cafe-flora-angular

**Getting Started**

There is no setup required to use this application.  Simply go to http://cafeflorafrontend.s3-website.us-east-2.amazonaws.com/login.  

**Usage**

There are screen shots available in the screenshots folder.  You start at the login page where you can create an account, recover a lost password, or log in if you have an account already.  If you don't want to create an account, you can log in using "email@email.com" and "password".  Once logged in you will be at the home page where you can select Menu from the navbar.  There you can add items to your cart.  I recommend the breakfast burrito.  Click add multiple times to add multiples of the same item.  You can click on Go To Cart to access your cart.  In the cart page you can add more of an item, remove it, clear the cart entirely, or click the Checkout button.  Checkout allows you to enter a coupon code.  One such code is HALFOFFBURRITO which will only work if you ordered the breakfast burrito.  Checkout also allows you set a delivery tip and review the total cost of your order.  Clicking proceed to payment sends you to the payment page where you enter your credit card information or use saved credit card information.  Clicking process payment takes you to the verification screen telling you when your order will be ready for pickup or delivered.  

**Contributors**

- Daniel Butcher as Team Lead
- Carlos Guerrero as Developer
- James Barlowe as Developer
- Jugen Fornoles as Developer
- Maxwell Dimgba as Developer
