package com.projectcafeflora.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectcafeflora.dao.MenuItemDao;
import com.projectcafeflora.dao.OrderDao;
import com.projectcafeflora.dao.OrderItemDao;
import com.projectcafeflora.model.MenuItem;
import com.projectcafeflora.model.Order;
import com.projectcafeflora.model.OrderItem;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class OrderItemService {
	
	OrderItemDao oiDao;
	OrderDao orDao;
	MenuItemDao miDao;
	
	public void insertOrderItem(int orderId, int menuItemId, int quantity, String specialInstructions) {
		Order order = orDao.findByOrderId(orderId);
		MenuItem menuItem = miDao.findByItemId(menuItemId);
		OrderItem orderItem = new OrderItem(order, menuItem, quantity, specialInstructions);
		oiDao.save(orderItem);
	}
	
	public OrderItem getById(int orderItemId) {
		return oiDao.findByOrderItemId(orderItemId);
	}
	
	public List<OrderItem> getByOrderId(int orderId) {
		Order order = orDao.findByOrderId(orderId);
		return oiDao.findByOrder(order);
	}
	
	//New
	public OrderItem getByOrderAndMenuItem(int orderId, int menuItemId) {
		Order order = orDao.findByOrderId(orderId);
		MenuItem menuItem = miDao.findByItemId(menuItemId);
		return oiDao.findByOrderAndMenuItem(order, menuItem);
	}
	
	//New
	public void updateOrderItem(int orderItemId, int quantity, String specialInstructions) {
		if(oiDao.existsById(orderItemId)) {
		OrderItem orderItem = oiDao.findByOrderItemId(orderItemId);
		orderItem.setQuantity(quantity);
		orderItem.setSpecialInstructions(specialInstructions);
		oiDao.save(orderItem);
		}
	}
	
	public void deleteById(int orderItemId) {
		OrderItem orderItem = oiDao.findByOrderItemId(orderItemId);
		oiDao.delete(orderItem);
	}
	
	public void deleteByOrderId(int orderId) {
		Order order = orDao.findByOrderId(orderId);
		oiDao.deleteByOrder(order);
	}

	public void insertOrderItem(OrderItem orderItem) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
}
