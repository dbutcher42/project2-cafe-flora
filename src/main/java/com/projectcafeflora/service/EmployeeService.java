package com.projectcafeflora.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectcafeflora.dao.EmployeeDao;
import com.projectcafeflora.model.Employee;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
public class EmployeeService {
	
	private EmployeeDao employeeDao;
	
}
