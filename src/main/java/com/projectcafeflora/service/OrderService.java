package com.projectcafeflora.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectcafeflora.ProjectCafeFloraApplication;
import com.projectcafeflora.dao.CouponDao;
import com.projectcafeflora.dao.CustomerDao;
import com.projectcafeflora.dao.DistributionDao;
import com.projectcafeflora.dao.OrderDao;
import com.projectcafeflora.dao.OrderStatusDao;
import com.projectcafeflora.model.Customer;
import com.projectcafeflora.model.Order;
import com.projectcafeflora.model.OrderStatus;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class OrderService {
	private OrderDao orDao;
	private CustomerDao crDao;
	private OrderStatusDao osDao;
	private CouponDao cnDao;
	private DistributionDao dnDao;
	
	public OrderService(OrderDao orDao) {
		super();
		this.orDao = orDao;
	}
	
	public OrderService(OrderDao orDao, CustomerDao custDao, OrderStatusDao osDao) {
		super();
		this.orDao = orDao;
		this.crDao = custDao;
		this.osDao = osDao;
	}
	
	public void insertNewOrder(int customerId) {
		Customer customer = crDao.findByCustomerId(customerId);
		OrderStatus orderStatus = osDao.findByStatusId(1);
		Order order = new Order(customer, orderStatus, 0.0);
		orDao.save(order);
	}
	
	public void updateStatusOrdered(int orderId, int distributionId, String couponCode, double subtotal, double tip, double tax, double total) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		Order order = orDao.findByOrderId(orderId);
		if (cnDao.findBycouponCode(couponCode)!=null) {
			order.setCoupon(cnDao.findBycouponCode(couponCode));
		} else {
			order.setCoupon(null);
		}
		order.setDistribution(dnDao.findById(distributionId));
		order.setOrderedTimestamp(timestamp);
//		order.setOrderStatus(osDao.findByStatusId(2));
		order.setSubtotal(subtotal);
		order.setDeliveryTip(tip);
		order.setTax(tax);
		order.setTotal(total);
		orDao.save(order);
	}
	
//	New -- for checkout
	public void updateOrderTotal(int orderId, double subtotal, double tax, double total) {
		if(orDao.findByOrderId(orderId) != null) {
			Order order = orDao.findByOrderId(orderId);
			order.setSubtotal(subtotal);
			order.setTax(tax);
			order.setTotal(total);
			orDao.save(order);
		}
	}
	
	public void updateStatusPreparing(int orderId) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		Order order = orDao.findByOrderId(orderId);
		order.setPreparingTimestamp(timestamp);
//		order.setOrderStatus(osDao.findByStatusId(3));
		orDao.save(order);
	}
	
	public void updateStatusReady(int orderId) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		Order order = orDao.findByOrderId(orderId);
		order.setReadyTimestamp(timestamp);
//		order.setOrderStatus(osDao.findByStatusId(4));
		orDao.save(order);
	}
	
	public void updateStatusCompleted(int orderId) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		Order order = orDao.findByOrderId(orderId);
		order.setCompletedTimestamp(timestamp);
		order.setOrderStatus(osDao.findByStatusId(6));
		String email = order.getCustomer().getEmail();
		ProjectCafeFloraApplication.log.info("Order entered for : " + email);
		orDao.save(order);
	}
	
	
	public void updateStatusDelivering(int orderId) {
		Order order = orDao.findByOrderId(orderId);
		order.setOrderStatus(osDao.findByStatusId(5));
		orDao.save(order);
	}
	
//	TESTED
	public void updateStatusCanceled(int orderId) {
		Order order = orDao.findByOrderId(orderId);
		order.setOrderStatus(osDao.findByStatusId(7));
		orDao.save(order);
	}
	
//	TESTED
	public void deleteById(int orderId) {
		Order order = orDao.findByOrderId(orderId);
		orDao.delete(order);
	}
	
//	TESTED
	public Order getById(int orderId) {
		return orDao.findByOrderId(orderId);
	}
	
//	TESTED
	public List<Order> getByStatus(int orderStatusId) {
		OrderStatus orderStatus = osDao.findByStatusId(orderStatusId);
		return orDao.findByOrderStatus(orderStatus);
	}
	
//	TESTED
	public List<Order> getAll() {
		return orDao.findAll();
	}

//	TESTED
	//this gets the new order created for a customer that has a status of 1
	public List<Order> getByStatusAndCustomer(int customerId) {
		OrderStatus orderStatus = osDao.findByStatusId(1);
		Customer customer = crDao.findByCustomerId(customerId);
		return orDao.findByOrderStatusAndCustomer(orderStatus, customer);
	}
	
}
