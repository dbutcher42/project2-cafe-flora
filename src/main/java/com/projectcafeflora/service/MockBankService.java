package com.projectcafeflora.service;

import java.util.List;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectcafeflora.dao.MockBankDao;
import com.projectcafeflora.model.MockBank;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class MockBankService {
	
	private MockBankDao mockBankDao;
	
	// insert the card number
	public void insertMockBank(MockBank bank) {
		mockBankDao.save(bank);
	}
	
	// get the number from credit card
	public MockBank getCreditCardByNumber(String number) {
		return mockBankDao.findByCreditCardNumber(number);
	}
	
	// get the name from the credit card
	public MockBank getCreditCardByName(String name) {
		return mockBankDao.findByCreditCardName(name);
	}
	
	// get the security code from the credit card
	public List<MockBank> getCreditCardByCode(int code) {
		return mockBankDao.findByCreditCardCode(code);
	}
	
	// get the amount from the credit card
	public List<MockBank> getCreditCardByAmount(double balance){
		return mockBankDao.findByCreditAmount(balance);
	}
	
	
}
