package com.projectcafeflora.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectcafeflora.dao.MenuItemDao;
import com.projectcafeflora.model.MenuItem;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class MenuItemService {
	
	private MenuItemDao miDao;
	
	public MenuItem getById(int menuItemId) {
		return miDao.findByItemId(menuItemId);
	}
	
	public List<MenuItem> getAll() {
		return miDao.findAll();
	}
	
	public void insertMenuItem(MenuItem menuItem) {
//	public void insertMenuItem(MenuItem menuItem) {
		miDao.save(menuItem);
	}
}
