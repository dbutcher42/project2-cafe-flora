package com.projectcafeflora.service;

import java.util.*; 
import javax.mail.*; 
import javax.mail.internet.*;
import javax.activation.*; 
import javax.mail.Session; 
import javax.mail.Transport; 

public class EmailService {	
	public static void sendPasswordEmail(String password, String firstName, String lastName, String email) {
		final String server_username = "CafeFloraEmail@gmail.com";
        final String server_password = "Cafeflora!";

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
          new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(server_username, server_password);
            }
          });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("CafeFloraEmail@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(email));
            message.setSubject("Password Recovery for Cafe Flora Email");
            message.setText("Dear our beloved customer " + firstName + " " + lastName + ", "
            		+ "we have recieved the request to recover your account's password : " + password);

            Transport.send(message);
            
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
	}
	
}
