package com.projectcafeflora.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter						
@Setter						
@NoArgsConstructor			
@AllArgsConstructor			
@ToString					 
@Entity
@Table(name="coupon_status")
public class CouponStatus {
	
	@Id
	@Column(name="coupon_status_id")
	@Setter(AccessLevel.NONE)
	private int couponStatusId;
	
	@Column(name="coupon_status_name", nullable=false)
	private String couponStatusName;
	
	public CouponStatus(String couponStatusName) {
		super();
		this.couponStatusName = couponStatusName;
	}
	
	
}
