package com.projectcafeflora.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter						
@Setter						
@NoArgsConstructor			
@AllArgsConstructor			
@ToString					 
@Entity
@Table(name="coupon")
public class Coupon implements Serializable {

	@Id
	@Column(name="coupon_code")
	@Setter(AccessLevel.NONE)
	private String couponCode;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="itemId")
	private MenuItem menuItem;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="couponStatusId")
	private CouponStatus couponStatus;
	
	@Column(name="coupon_amount")
	private double couponAmount;
	
//	Coupon constructor for non-specific Items
	public Coupon(double amount, CouponStatus couponStatus) {
		super();
		this.couponAmount = amount;
		this.couponStatus = couponStatus;
	}
	
//	Coupon constructor for specific items
	public Coupon(double amount,  CouponStatus couponStatus, MenuItem menuItem) {
		super();
		this.couponAmount = amount;
		this.couponStatus = couponStatus;
		this.menuItem = menuItem;
	}
}
