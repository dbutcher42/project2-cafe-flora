package com.projectcafeflora.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter						
@Setter						
@NoArgsConstructor			
@AllArgsConstructor			
@ToString					 
@Entity
@Table(name="mock_bank")
public class MockBank {

	@Id
	@Column(name="credit_card_number")
	private String creditCardNumber;
	
	@Column(name="credit_amount", nullable=false)
	private double creditAmount;
	
	@Column(name="credit_card_name", nullable=false)
	private String creditCardName;
	
	@Column(name="credit_card_code", nullable=false)
	private int creditCardCode;
	
	@Column(name="credit_card_month", nullable=false)
	private int creditCardMonth;
	
	@Column(name="credit_card_year", nullable=false)
	private int creditCardYear;
	
}
