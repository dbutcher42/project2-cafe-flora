package com.projectcafeflora.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter						
@Setter						
@NoArgsConstructor			
@AllArgsConstructor			
@ToString					 
@Entity
@Table(name="order_status")
public class OrderStatus implements Serializable {
	
	@Id
	@Column(name="status_id")
	@Setter(AccessLevel.NONE)
	private int statusId;
	
	@Column(name="status_name", nullable=false)
	private String statusName;
	
}
