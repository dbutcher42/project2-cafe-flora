package com.projectcafeflora.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projectcafeflora.model.Distribution;

public interface DistributionDao extends JpaRepository<Distribution, Integer>{

	public List<Distribution> findAll();
	public Distribution findById(int id);
	public Distribution findByDistributionMethod(String distMethod);
	
}
