package com.projectcafeflora.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projectcafeflora.model.OrderStatus;

public interface OrderStatusDao extends JpaRepository<OrderStatus, Integer> {
	
	public OrderStatus findByStatusId(int statusId);
	
}
