package com.projectcafeflora.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projectcafeflora.model.Coupon;

//public interface CouponDao extends JpaRepository<Coupon, Integer>{
public interface CouponDao extends JpaRepository<Coupon, String>{

	public Coupon findBycouponCode(String couponCode);
//	public Coupon findById(int id);
	
	public List<Coupon> findByCouponAmount(double couponAmount);
	
	public List<Coupon> findAll();

}
