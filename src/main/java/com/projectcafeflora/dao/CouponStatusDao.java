package com.projectcafeflora.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projectcafeflora.model.CouponStatus;

public interface CouponStatusDao extends JpaRepository<CouponStatus, Integer>{
	
	public CouponStatus findById(int id);
	
	public List<CouponStatus> findAll();

}
