package com.projectcafeflora.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projectcafeflora.model.Customer;

public interface CustomerDao extends JpaRepository<Customer, Integer>{

	public Customer findByCustomerId(int customerId);
	public Customer findByPhone(String phone);
	public Customer findByEmail(String email);
	public Customer findByEmailAndPassword(String email, String password);
}
