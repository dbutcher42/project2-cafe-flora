package com.projectcafeflora.controller;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projectcafeflora.model.Coupon;
import com.projectcafeflora.service.CouponService;
import com.projectcafeflora.service.CouponStatusService;
import com.projectcafeflora.service.OrderService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/coupons")
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class CouponController {
	
	private CouponService cServ;
	private CouponStatusService csServ;
	
//	Insert Methods
	@PostMapping()
	public ResponseEntity<String> insertCoupon(@RequestBody LinkedHashMap<String, String> cMap){
//	public ResponseEntity<String> insertCoupon(@RequestBody Coupon coupon){
		String couponCode = cMap.get("couponCode");
		int menuItemId = Integer.parseInt(cMap.get("menuItemId"));
		int couponStatusId = Integer.parseInt(cMap.get("couponStatusId"));
		double couponAmount = Double.parseDouble(cMap.get("couponAmount"));
		cServ.insertNewCoupon(couponCode, menuItemId, couponStatusId, couponAmount);
		return new ResponseEntity<>("Coupon was successfully created", HttpStatus.CREATED);
	}
	

	//used by daniel
	@GetMapping("/individualcoupons/{couponcode}")
	public ResponseEntity<Coupon> getCouponById(@PathVariable("couponcode") String couponCode){
		if(cServ.getByCouponCode(couponCode) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(cServ.getByCouponCode(couponCode), HttpStatus.OK);
	}
	

	


}

