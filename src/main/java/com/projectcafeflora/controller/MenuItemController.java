package com.projectcafeflora.controller;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projectcafeflora.model.MenuItem;
import com.projectcafeflora.service.MenuItemService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/menu-items")
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class MenuItemController {
	
	MenuItemService miServ = new MenuItemService();
	
	@PostMapping()
	public ResponseEntity<String> insertMenuItem(@RequestBody LinkedHashMap<String, String> xMap) {
		MenuItem menuItem = new MenuItem(xMap.get("itemName"), xMap.get("itemDescription"), Double.parseDouble(xMap.get("itemCost")), xMap.get("itemImageUrl"));
		miServ.insertMenuItem(menuItem);
		return new ResponseEntity<>("Menu Item was created", HttpStatus.CREATED);
	}
	
	@GetMapping("/allmenuitems")
	public ResponseEntity<List<MenuItem>> getAllMenuItems() {
		if (miServ.getAll() == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(miServ.getAll(), HttpStatus.OK);
	}
	
	@GetMapping("/individualmenuitems/{menuitemid}")
	public ResponseEntity<MenuItem> getMenuItemById(@PathVariable("menuitemid") String menuitemid) {
		int id = Integer.parseInt(menuitemid);
		if (miServ.getById(id) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(miServ.getById(id), HttpStatus.OK);
	}

}
