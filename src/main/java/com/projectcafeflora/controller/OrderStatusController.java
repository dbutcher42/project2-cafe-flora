package com.projectcafeflora.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projectcafeflora.model.OrderStatus;
import com.projectcafeflora.service.OrderStatusService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/order-statuses")
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class OrderStatusController {
	
	private OrderStatusService osServ;
	
	@GetMapping("/{orderstatusid}")
	public ResponseEntity<OrderStatus> getOrderStatusById(@PathVariable("orderstatusid") String orderStatusId) {
		int id = Integer.parseInt(orderStatusId);
		if (osServ.getById(id) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(osServ.getById(id), HttpStatus.OK);
	}
}
