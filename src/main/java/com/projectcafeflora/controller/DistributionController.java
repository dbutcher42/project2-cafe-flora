package com.projectcafeflora.controller;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projectcafeflora.model.Distribution;
import com.projectcafeflora.service.DistributionService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/distributions")
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
public class DistributionController {

	private DistributionService distServ;
	
	@GetMapping("/individualdistributions/{distid}")
	public ResponseEntity<Distribution> getDistributionById(@PathVariable("distid") String id) {
		Integer dId = Integer.parseInt(id);
		if(distServ.getDistributionById(dId) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(distServ.getDistributionById(dId), HttpStatus.OK);
	}
	
}
