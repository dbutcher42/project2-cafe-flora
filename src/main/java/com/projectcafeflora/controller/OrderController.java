package com.projectcafeflora.controller;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projectcafeflora.model.Order;
import com.projectcafeflora.service.OrderService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/orders")
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class OrderController {
	private OrderService orServ = new OrderService();
	
//	
	@PostMapping("/neworders/{customerid}")
	public ResponseEntity<String> insertNewOrder(@PathVariable("customerid") String customerId) {
		orServ.insertNewOrder(Integer.parseInt(customerId));
		return new ResponseEntity<>("New Order was created", HttpStatus.CREATED);
	}
	
//	NEEDS WORK
	@PostMapping("/orderedorders")
	public ResponseEntity<String> updateOrderedOrder(@RequestBody Order order) {
		int orderId = order.getOrderId();
		int distributionId = order.getDistribution().getDistributionId();
		String couponCode;
		try {
			couponCode = order.getCoupon().getCouponCode();
		}catch(NullPointerException e) {
			couponCode = null;
		}
		double tax = order.getTax();
		double tip = order.getDeliveryTip();
		double total = order.getTotal();
		double subtotal = order.getSubtotal();
		orServ.updateStatusOrdered(orderId, distributionId, couponCode, subtotal, tip, tax, total);
		return new ResponseEntity<>("Order is Ordered", HttpStatus.OK);
	}
	
//	TESTED
	@PostMapping("/preparingorders/{orderid}")
	public ResponseEntity<String> updatePreparingOrder(@PathVariable("orderid") String orderId) {
		int id = Integer.parseInt(orderId);
		orServ.updateStatusPreparing(id);
		return new ResponseEntity<>("Order is Preparing", HttpStatus.OK);
	}
	
//	TESTED
	@PostMapping("/readyorders/{orderid}")
	public ResponseEntity<String> updateReadyOrder(@PathVariable("orderid") String orderId) {
		int id = Integer.parseInt(orderId);
		orServ.updateStatusReady(id);
		return new ResponseEntity<>("Order is Ready", HttpStatus.OK);
	}
	
//	TESTED
	@PostMapping("/completedorders/{orderid}")
	public ResponseEntity<String> updateCompletedOrder(@PathVariable("orderid") String orderId) {
		int id = Integer.parseInt(orderId);
		orServ.updateStatusCompleted(id);
		return new ResponseEntity<>("Order is Completed", HttpStatus.OK);
	}
	
//	TESTED
	@PostMapping("/deliveryorders/{orderid}")
	public ResponseEntity<String> updateDeliveryingOrder(@PathVariable("orderid") String orderId) {
		int id = Integer.parseInt(orderId);
		orServ.updateStatusDelivering(id);
		return new ResponseEntity<>("Order is Delivering", HttpStatus.OK);
	}
	
//	TESTED
	@PostMapping("/canceledorders/{orderid}")
	public ResponseEntity<String> updateCanceledOrder(@PathVariable("orderid") String orderId) {
		int id = Integer.parseInt(orderId);
		orServ.updateStatusCanceled(id);
		return new ResponseEntity<>("Order is Canceled", HttpStatus.OK);
	}

//	TESTED
	@PutMapping("/addTotal")
	public ResponseEntity<String> updateOrderTotal(@RequestBody LinkedHashMap<String, String> xMap){
		
		int orderId = Integer.parseInt(xMap.get("orderId"));
		double subtotal = Double.parseDouble(xMap.get("subtotal"));
		double tax = Double.parseDouble(xMap.get("tax"));
		double total = Double.parseDouble(xMap.get("total"));
		
		orServ.updateOrderTotal(orderId, subtotal, tax, total);
		return new ResponseEntity<>("Totals were updated", HttpStatus.OK);
	}
	
//	Tested
	@DeleteMapping("/{orderid}")
	public ResponseEntity<String> deleteOrderById(@PathVariable("orderid") String orderId) {
		orServ.deleteById(Integer.parseInt(orderId));
		return new ResponseEntity<>("Order is Deleted", HttpStatus.OK);
	}
	
//	TESTED
	@GetMapping("/individualorders/{orderid}")
	public ResponseEntity<Order> getOrderById(@PathVariable("orderid") String orderId) {
		int id = Integer.parseInt(orderId);
		if (orServ.getById(id) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(orServ.getById(id), HttpStatus.OK);
	}
	
//	TESTED
	@GetMapping("/statusorders/{orderstatusid}")
	public ResponseEntity<List<Order>> getOrderByStatus(@PathVariable("orderstatusid") String orderStatusId) {
		int id = Integer.parseInt(orderStatusId);
		if (orServ.getByStatus(id) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(orServ.getByStatus(id), HttpStatus.OK);
	}
	
//	TESTED
	@GetMapping("/allorders")
	public ResponseEntity<List<Order>> getAllOrders() {
		if (orServ.getAll() == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(orServ.getAll(), HttpStatus.OK);
	}
	
//	TESTED
	@GetMapping("/neworders/{customerid}")
	public ResponseEntity<Order> getOrderByStatusAndCustomer(@PathVariable("customerid") String customerId) {
		int id = Integer.parseInt(customerId);
		if (orServ.getByStatus(id) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		List<Order> orderList = orServ.getByStatusAndCustomer(id);
		Order order = orderList.get(0);
		return new ResponseEntity<>(order, HttpStatus.OK);
	}
		
}
