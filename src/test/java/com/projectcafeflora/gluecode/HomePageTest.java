package com.projectcafeflora.gluecode;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.projectcafeflora.eval.page.CartPage;
import com.projectcafeflora.eval.page.CheckOutPage;
import com.projectcafeflora.eval.page.HomePage;
import com.projectcafeflora.eval.page.LoginPage;
import com.projectcafeflora.eval.page.MenuPage;
import com.projectcafeflora.eval.page.PaymentPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class HomePageTest {
	
	public LoginPage loginPage;
	public HomePage homePage;
	public MenuPage menuPage;
	public CartPage cartPage;
	public CheckOutPage checkoutPage;
	public PaymentPage paymentPage;
	public WebDriver driver = new ChromeDriver();
	
	@Given("a user clicks the home button first time")
	public void a_user_clicks_the_home_button_first_time() {
	    this.homePage = new HomePage(DriverUtility.driver);
		this.homePage.clickHomeButton();
	}
	
	@When("a user clicks the home button")
	public void a_user_clicks_the_home_button() {
		this.homePage = new HomePage(DriverUtility.driver);
		this.homePage.clickHomeButton();
	}
	@Then("a user is redirected to the home page")
	public void a_user_is_redirected_to_the_home_page() throws InterruptedException {
	    this.homePage = new HomePage(DriverUtility.driver);
	    TimeUnit.SECONDS.sleep(3);
	}
	
	@Given("a user clicks the menu button first time")
	public void a_user_clicks_the_menu_button_first_time() {
	   this.homePage = new HomePage(DriverUtility.driver);
	   this.homePage.clickMenuButton();
	}
	
	@When("user clicks the menu button")
	public void user_clicks_the_menu_button() {
	    this.homePage.clickMenuButton();
	}
	@Then("a user is redirected to the menu page")
	public void a_user_is_redirected_to_the_menu_page() throws InterruptedException {
	    this.homePage = new HomePage(DriverUtility.driver);
	    TimeUnit.SECONDS.sleep(3);
	}
	
	@Given("a user clicks the cart button first time")
	public void a_user_clicks_the_cart_button_first_time() {
	    this.homePage = new HomePage(DriverUtility.driver);
	    this.homePage.clickCartButton();
	}
	
//	@When("then the user submits the informationAnd")
//	public void then_the_user_submits_the_information_and() {
//	    this.homePage.clickCartButton();
//	}
	
	@When("a user clicks the cart button")
	public void a_user_clicks_the_cart_button() {
	    this.homePage.clickCartButton();
	}

	@Then("a user is redirected to the cart page")
	public void a_user_is_redirected_to_the_cart_page() throws InterruptedException {
		this.homePage = new HomePage(DriverUtility.driver);
	    TimeUnit.SECONDS.sleep(3);
	}
	
	@Given("a user clicks the project button first time")
	public void a_user_clicks_the_project_button_first_time() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}

	@When("a user clicks the project button")
	public void a_user_clicks_the_project_button() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}
		
	@Then("a user is redirected to the new page")
	public void a_user_is_redirected_to_the_new_page() throws InterruptedException {
		this.homePage = new HomePage(DriverUtility.driver);
	    TimeUnit.SECONDS.sleep(3);
	}
	
//	@Given("a user clicks the about us button first time")
//	public void a_user_clicks_the_about_us_button_first_time() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
//
//	@When("a user clicks the about us button")
//	public void a_user_clicks_the_about_us_button() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
	
	// this error is because of duplicated!
//	@Then("a user is redirected to the new page")
//	public void a_user_is_redirected_to_the_new_page() {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
//	}
}
