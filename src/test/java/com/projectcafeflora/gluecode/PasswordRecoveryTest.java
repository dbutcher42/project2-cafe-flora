package com.projectcafeflora.gluecode;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import com.projectcafeflora.eval.page.LoginPage;
import com.projectcafeflora.eval.page.PasswordRecoveryPage;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PasswordRecoveryTest {
	
	public LoginPage loginPage;
	public PasswordRecoveryPage recoveryPage;
	
	@When("a user inputs their email into the recover password field {string}")
	public void a_user_inputs_their_email_into_the_recover_password_field(String string) {
		this.recoveryPage = new PasswordRecoveryPage(DriverUtility.driver);
		recoveryPage.setEmail(string);
	}

	@When("then a user inputs their first name {string}")
	public void then_a_user_inputs_their_first_name(String string) {
	    recoveryPage.setFirstName(string);
	}
	@When("then a user inputs their last name {string}")
	public void then_a_user_inputs_their_last_name(String string) {
	    recoveryPage.setLastName(string);
	}
	@When("then the user clicks the send link button")
	public void then_the_user_clicks_the_send_link_button() {
	    recoveryPage.submit();
	}
	@Then("the user is redirected back to the login page")
	public void the_user_is_redirected_back_to_the_login_page() throws InterruptedException {
		TimeUnit.SECONDS.sleep(3);
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/login");
	}
	
	@Then("the user gets a bad email message")
	public void the_user_gets_a_bad_email_message() throws InterruptedException {
		TimeUnit.SECONDS.sleep(3);
		assertEquals(recoveryPage.getMessage(), "Email was not a valid email try again");
	}
	
	@When("then the user clicks the back to login button")
	public void then_the_user_clicks_the_back_to_login_button() {
		this.recoveryPage = new PasswordRecoveryPage(DriverUtility.driver);
		recoveryPage.backToLogin();
	}

}
