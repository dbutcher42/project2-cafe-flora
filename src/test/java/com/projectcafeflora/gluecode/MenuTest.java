package com.projectcafeflora.gluecode;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.projectcafeflora.eval.page.CartPage;
import com.projectcafeflora.eval.page.CheckOutPage;
import com.projectcafeflora.eval.page.HomePage;
import com.projectcafeflora.eval.page.LoginPage;
import com.projectcafeflora.eval.page.MenuPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class MenuTest {
	
		public LoginPage loginPage;
		public HomePage homePage;
		public MenuPage menuPage;
		public CartPage cartPage;
		public WebDriver driver = new ChromeDriver();

//		Pt. 1
//		@When("a user finds an item Coffee")
//		public void a_user_finds_an_item_coffee() {
//		    // Write code here that turns the phrase above into concrete actions
//		    //this.mp = new MenuPage(DriverUtility.driver);
//		}
//
//		
//		@Then("the item is visible")
//		public void the_item_is_visible() {
//		    // Write code here that turns the phrase above into concrete actions
//		    throw new io.cucumber.java.PendingException();
//		}

//		Pt. 2
		@When("a user clicks add to cart for Burrito")
		public void a_user_clicks_add_to_cart_for_burrito() throws InterruptedException{
		    // Write code here that turns the phrase above into concrete actions
			this.menuPage = new MenuPage(DriverUtility.driver);
			TimeUnit.SECONDS.sleep(3);
			assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/menu");
			this.menuPage.clickAddBurritoButton();
			TimeUnit.SECONDS.sleep(3);
			
			
		}


		@Then("a message will appear")
		public void a_message_will_appear() {
		    // Write code here that turns the phrase above into concrete actions
		    throw new io.cucumber.java.PendingException();
		}
	

//		Pt. 3
		@When("a user clicks add to cart for Bacon")
		public void a_user_clicks_add_to_cart_for_bacon() {
		    // Write code here that turns the phrase above into concrete actions
		    throw new io.cucumber.java.PendingException();
		}

		@When("a user clicks add to cart for Coffee")
		public void a_user_clicks_add_to_cart_for_coffee() {
		    // Write code here that turns the phrase above into concrete actions
		    throw new io.cucumber.java.PendingException();
		}
		@When("a user clicks add to cart for Waffles")
		public void a_user_clicks_add_to_cart_for_waffles() {
		    // Write code here that turns the phrase above into concrete actions
		    throw new io.cucumber.java.PendingException();
		}

//		Pt. 4
		@Given("a user is at the login page of Cafe Flora fourth time")
		public void a_user_is_at_the_login_page_of_cafe_flora_fourth_time() {
		    // Write code here that turns the phrase above into concrete actions
		    throw new io.cucumber.java.PendingException();
		}

		@When("a user clicks add to cart for Burrito three times")
		public void a_user_clicks_add_to_cart_for_burrito_three_times() throws InterruptedException {
		    // Write code here that turns the phrase above into concrete actions
		    this.menuPage = new MenuPage(DriverUtility.driver);
		    TimeUnit.SECONDS.sleep(3);
			assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/menu");
		    this.menuPage.clickAddBurritoButton();
			TimeUnit.SECONDS.sleep(3);
			this.menuPage.clickAddBurritoButton();
			TimeUnit.SECONDS.sleep(3);
			this.menuPage.clickAddBurritoButton();
		}
		@When("the user clicks on go to cart")
		public void the_user_clicks_on_go_to_cart() throws InterruptedException {
		    // Write code here that turns the phrase above into concrete actions
			this.cartPage = new CartPage(DriverUtility.driver);
			assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/cart");
			
			
		}
		@Then("the user is redirected to the cart")
		public void the_user_is_redirected_to_the_cart() throws InterruptedException {
		    // Write code here that turns the phrase above into concrete actions
		    throw new io.cucumber.java.PendingException();
		}








}
