package com.projectcafeflora.gluecode;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.projectcafeflora.eval.page.CartPage;
import com.projectcafeflora.eval.page.CheckOutPage;
import com.projectcafeflora.eval.page.HomePage;
import com.projectcafeflora.eval.page.LoginPage;
import com.projectcafeflora.eval.page.MenuPage;
import com.projectcafeflora.eval.page.PaymentPage;
import com.projectcafeflora.eval.page.VerificationPage;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PaymentTest {

	public LoginPage loginPage;
	public HomePage homePage;
	public MenuPage menuPage;
	public CartPage cartPage;
	public CheckOutPage checkoutPage;
	public PaymentPage paymentPage;
	public VerificationPage verifyPage;
	public WebDriver driver = new ChromeDriver();
	
	@When("a user inputs their name {string}")
	public void a_user_inputs_their_name(String string) {
		this.paymentPage = new PaymentPage(DriverUtility.driver);
	    this.paymentPage.setCcName(string);
	}

	@When("a user inputs their cc number {string}")
	public void a_user_inputs_their_cc_number(String string) {
	    this.paymentPage.setCcNumber(string);
	}

	@When("a user inputs their cc month {string}")
	public void a_user_inputs_their_cc_month(String string) {
	    this.paymentPage.setCcMonth(string);
	}
	
	@When("a user inputs their cc year {string}")
	public void a_user_inputs_their_cc_year(String string) {
	    this.paymentPage.setCcYear(string);
	}
	
	@When("a user inputs their cc code {string}")
	public void a_user_inputs_their_cc_code(String string) {
	    this.paymentPage.setCcCode(string);
	}
	
	@When("then a user clicks process payment")
	public void then_a_user_clicks_process_payment() {
	    this.paymentPage.clickSubmit();
	}
	
	@Then("a user is redirected to the verification page")
	public void a_user_is_redirected_to_the_verification_page() throws InterruptedException {
	    this.verifyPage = new VerificationPage(DriverUtility.driver);
	    TimeUnit.SECONDS.sleep(3);
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/verified");
	}
	
	@When("then a user clicks on use saved CC info")
	public void then_a_user_clicks_on_use_saved_CC_info() throws InterruptedException {
		this.paymentPage = new PaymentPage(DriverUtility.driver);
		this.paymentPage.clickUseSaved();
	    TimeUnit.SECONDS.sleep(3);
	    assertEquals(this.paymentPage.getCcName(), "Daniel Butcher");
	    assertEquals(this.paymentPage.getCcNumber(), "1111111111111111");
	    assertEquals(this.paymentPage.getCcMonth(), "1");
	    assertEquals(this.paymentPage.getCcYear(), "2025");
	    assertEquals(this.paymentPage.getCcCode(), "999");
	}
}
