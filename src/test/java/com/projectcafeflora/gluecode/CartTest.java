package com.projectcafeflora.gluecode;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.projectcafeflora.eval.page.CartPage;
import com.projectcafeflora.eval.page.HomePage;
import com.projectcafeflora.eval.page.LoginPage;
import com.projectcafeflora.eval.page.MenuPage;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CartTest {

	public CartPage cp;
	public MenuPage mp;
	public LoginPage lp;
	public HomePage hp;
//	public CheckOutPage cop;
//	public PaymentPage pp;
	public WebDriver driver = new ChromeDriver();

//	
	@When("a user clicks the add bacon button")
	public void a_user_clicks_the_add_bacon_button() throws InterruptedException{
		// Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
		this.mp = new MenuPage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(3);
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/menu");
		try {
			this.mp.clickAddBaconButton();
		} catch (UnhandledAlertException f) {
			try {
				Alert alert = driver.switchTo().alert();
				alert.accept();
			} catch (NoAlertPresentException e) {
				e.printStackTrace();
			}
		}
	}

	@When("then a user clicks the clear cart button")
	public void then_a_user_clicks_the_clear_cart_button() throws InterruptedException{
		// Write code here that turns the phrase above into concrete actions
//	    throw new io.cucumber.java.PendingException();
		this.cp = new CartPage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(3);
		this.cp.clickClearCartBtn();
	}
	
	@Then("the cart total should be 0")
	public void the_cart_total_should_be_0() {
		this.cp = new CartPage(DriverUtility.driver);
		assertEquals(this.cp.getCartTotal(), "$0.00");		
	}
	
	@When("then a user clicks the remove button")
	public void then_a_user_clicks_the_remove_button() throws InterruptedException{
		this.cp = new CartPage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(3);
		this.cp.clickRemoveItemBtn();
		
	}
	
	@Then("the cart total should decrease")
	public void the_cart_total_should_decrease() throws InterruptedException{
		this.cp = new CartPage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(3);
		assertEquals(this.cp.getCartTotal(), "$4.73");
		
	}
	
	
	
	@When("then a user clicks the add button")
	public void then_a_user_clicks_the_add_button() throws InterruptedException{
		this.cp = new CartPage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(3);
		this.cp.clickAddItemBtn();

	}
	
	@Then("the cart total should increase")
	public void the_cart_total_should_increase() throws InterruptedException{
		this.cp = new CartPage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(3);
		assertEquals(this.cp.getCartTotal(), "$16.80");
		
	}

}


