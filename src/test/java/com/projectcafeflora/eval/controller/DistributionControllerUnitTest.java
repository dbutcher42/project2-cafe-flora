package com.projectcafeflora.eval.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.projectcafeflora.controller.DistributionController;
import com.projectcafeflora.model.Distribution;
import com.projectcafeflora.service.DistributionService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
public class DistributionControllerUnitTest {
	
	@Mock
	private DistributionService dServ;
	
	@InjectMocks
	private DistributionController dCon;
	
	private Distribution distribution;
	
	private MockMvc mock;
	
	@BeforeEach
	public void setUp() throws Exception {
		distribution = new Distribution(1, "Delivery");
		mock = MockMvcBuilders.standaloneSetup(dCon).build();
		when(dServ.getDistributionById(1)).thenReturn(distribution);
	}
	
	@Test
	public void getDistributionTest() throws Exception {
		mock.perform(get("/distributions/individualdistributions/{distid}", distribution.getDistributionId()))
			.andExpect(status().isOk());
	
	}

}
