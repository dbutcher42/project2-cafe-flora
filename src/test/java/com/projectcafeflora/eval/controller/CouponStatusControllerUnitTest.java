package com.projectcafeflora.eval.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.projectcafeflora.controller.CouponStatusController;
import com.projectcafeflora.model.CouponStatus;
import com.projectcafeflora.service.CouponStatusService;

@SpringBootTest
public class CouponStatusControllerUnitTest {

	@Mock
	private CouponStatusService csServ;
	
	@InjectMocks
	private CouponStatusController csCon;
	
	private CouponStatus coupStatus;
	
	private MockMvc mock;
	
	@BeforeEach
	public void setUp() throws Exception {
		coupStatus = new CouponStatus(1,"Active");
		mock = MockMvcBuilders.standaloneSetup(csCon).build();
		when(csServ.getById(1)).thenReturn(coupStatus);
		when(csServ.getById(0)).thenReturn(null);
	}
	
	@Test
	public void getCouponStatusSuccess() throws Exception {
		mock.perform(get("/couponstatuses/{coupstatid}", coupStatus.getCouponStatusId()))
			.andExpect(status().isOk()).andExpect(jsonPath("$.couponStatusName", is(coupStatus.getCouponStatusName())));
	}
	
	@Test
	public void getCouponStatusFailure() throws Exception {
		mock.perform(get("/couponstatuses/{coupstatid}", 0))
			.andExpect(status().isNotFound());
	}
	
}
