package com.projectcafeflora.eval.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectcafeflora.controller.CouponController;
import com.projectcafeflora.model.Coupon;
import com.projectcafeflora.model.CouponStatus;
import com.projectcafeflora.model.MenuItem;
import com.projectcafeflora.service.CouponService;

@SpringBootTest
public class CouponControllerUnitTest {

	@Mock
	private CouponService cnServ;
	
	@InjectMocks
	private CouponController cnCon;
	
	private Coupon coupon;
	private CouponStatus couponStatus;
	private MenuItem menuItem;
	
	private MockMvc mock;
	
	@BeforeEach
	public void setUp() throws Exception {
		couponStatus = new CouponStatus(1,"Active");
		menuItem = new MenuItem(1,"Bacon", "Plate of nice crispy bacon", 4.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Bacon.png");
		coupon = new Coupon("baconspecial", menuItem, couponStatus, -2.00);
		mock = MockMvcBuilders.standaloneSetup(cnCon).build();
		when(cnServ.getByCouponCode("baconspecial")).thenReturn(coupon);
		doNothing().when(cnServ).insertNewCoupon("baconspecial", 1, 1, -2);
	}
	
	@Test
	public void getCouponSuccess() throws Exception {
		mock.perform(get("/coupons/individualcoupons/{couponcode}", coupon.getCouponCode()))
		.andExpect(status().isOk()).andExpect(jsonPath("$.couponCode", is(coupon.getCouponCode())))
		.andExpect(jsonPath("$.menuItem.itemId", is(coupon.getMenuItem().getItemId())))
		.andExpect(jsonPath("$.couponStatus.couponStatusId", is(coupon.getCouponStatus().getCouponStatusId())))
		.andExpect(jsonPath("$.couponAmount", is(coupon.getCouponAmount())));
				
	}
	
	@Test
	public void insertCouponSuccess() throws Exception {
		mock.perform(post("/coupons").contentType(MediaType.APPLICATION_JSON)
		.content("{\"couponCode\":\"baconspecial\",\"menuItemId\":\"1\",\"couponStatusId\":\"1\",\"couponAmount\":\"-2\"}"))
		.andExpect(status().isCreated()).andExpect(jsonPath("$").value("Coupon was successfully created"));
	}
	
}
