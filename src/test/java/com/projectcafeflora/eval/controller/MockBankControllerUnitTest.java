package com.projectcafeflora.eval.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.projectcafeflora.controller.MockBankController;
import com.projectcafeflora.model.MockBank;
import com.projectcafeflora.service.MockBankService;

@SpringBootTest
public class MockBankControllerUnitTest {
	
	@Mock
	private MockBankService mockbankServ;
	
	@InjectMocks
	private MockBankController mockbankCon;
	
	private MockBank mockbank;
	
	private MockMvc mock;
	
	/*
	 * In model, the mock bank includes:
	 * 	string: number
	 * 	double: amount
	 * 	string: name
	 * 	int   : code
	 * 	int	  : month
	 * 	int   : year
	 */
	@BeforeEach
	public void setUp() throws Exception{
		mockbank = new MockBank("1111111111111111", 5000.0, "Daniel Butcher", 999, 1, 2025);
		mock = MockMvcBuilders.standaloneSetup(mockbankCon).build();
		when(mockbankServ.getCreditCardByNumber("1111111111111111")).thenReturn(mockbank);
		when(mockbankServ.getCreditCardByName("Daniel Butcher")).thenReturn(mockbank);
//		when(mockbankServ.getCreditCardByCode(999)).thenReturn(mockbank);
	}
	
	@Test
	public void getCreditCardNumberSuccess() throws Exception {
		mock.perform(get("/mockbanks/creditcardnumbers/{creditcardnumber}", mockbank.getCreditCardNumber()))
			.andExpect(status().isOk()).andExpect(jsonPath("$.creditCardName", is(mockbank.getCreditCardName())));
	}


	@Test
	public void getCreditCardNameSuccess() throws Exception{
		mock.perform(get("/mockbanks/creditcardname/{creditcardnumber}", mockbank.getCreditCardName()))
		.andExpect(status().isOk()).andExpect(jsonPath("$.creditCardNumber", is(mockbank.getCreditCardNumber())));
	}

}
