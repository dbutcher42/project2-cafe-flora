package com.projectcafeflora.eval.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.projectcafeflora.dao.CouponStatusDao;
import com.projectcafeflora.model.CouponStatus;
import com.projectcafeflora.service.CouponStatusService;

@SpringBootTest
public class CouponStatusServiceUnitTest {

	@Mock
	CouponStatusDao csDao;
	
	@InjectMocks
	CouponStatusService csServ;
	
	CouponStatus couponStatus;
	
	@BeforeEach
	public void setUp() throws Exception {
		couponStatus = new CouponStatus(1, "Active");
		when(csDao.findById(1)).thenReturn(couponStatus);
		when(csDao.findById(0)).thenReturn(null);
	}
	
	@Test
	public void getByIdSuccess() {
		assertEquals(csServ.getById(1), couponStatus);
	}
	
	@Test
	public void getByIdFailure() {
		assertEquals(csServ.getById(0), null);
	}
	
}
