package com.projectcafeflora.eval.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.projectcafeflora.dao.CouponDao;
import com.projectcafeflora.dao.CouponStatusDao;
import com.projectcafeflora.dao.MenuItemDao;
import com.projectcafeflora.model.Coupon;
import com.projectcafeflora.model.CouponStatus;
import com.projectcafeflora.model.MenuItem;
import com.projectcafeflora.service.CouponService;

@SpringBootTest
public class CouponServiceUnitTest {

	@Mock
	private CouponDao cnDao;

	@Mock
	private CouponStatusDao csDao;
	
	@Mock
	private MenuItemDao miDao;
	
	@InjectMocks
	private CouponService cnServ;
	
	@InjectMocks
	private CouponService cnServ2 = mock(CouponService.class);
	
	Coupon coupon;
	CouponStatus couponStatus;
	MenuItem menuItem;
	
	@BeforeEach
	public void setUp() throws Exception {
		couponStatus = new CouponStatus(1, "Active");
		menuItem = new MenuItem(1,"Bacon", "Plate of nice crispy bacon", 4.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Bacon.png");
		coupon = new Coupon("baconspecial", menuItem, couponStatus, -2.00);
		when(cnDao.findBycouponCode("baconspecial")).thenReturn(coupon);
		when(cnDao.findBycouponCode("something")).thenReturn(null);
	}
	
	@Test
	public void getByCouponCodeSuccess() {
		assertEquals(cnServ.getByCouponCode("baconspecial"),coupon);
	}
	
	@Test
	public void getByCouponCodeFailure() {
		assertEquals(cnServ.getByCouponCode("something"),null);
	}
	
	@Test
	public void insertNewCouponSuccess() {
		cnServ2.insertNewCoupon("baconspecial", 1, 1, -2);
		verify(cnServ2, times(1)).insertNewCoupon("baconspecial", 1, 1, -2);
	}
}
