package com.projectcafeflora.eval.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.projectcafeflora.dao.DistributionDao;
import com.projectcafeflora.model.Distribution;
import com.projectcafeflora.service.DistributionService;

@SpringBootTest
public class DistributionServiceUnitTest {
	
	@Mock
	private DistributionDao dDao;
	
	@InjectMocks
	private DistributionService dServ;
	
	private Distribution distribution;
	
	@BeforeEach
	public void setUp() throws Exception {
		distribution = new Distribution(1, "Delivery");
		when(dDao.findById(1)).thenReturn(distribution);
		when(dDao.findById(3)).thenReturn(null);
	}
	
	@Test
	public void testFindByIdSuccess() {
		assertEquals(dServ.getDistributionById(1), distribution);
	}
	
	@Test
	public void testFindByIdFail() {
		assertEquals(dServ.getDistributionById(3), null);
	}
}
