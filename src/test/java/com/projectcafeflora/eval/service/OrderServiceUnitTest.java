package com.projectcafeflora.eval.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.projectcafeflora.dao.CustomerDao;
import com.projectcafeflora.dao.OrderDao;
import com.projectcafeflora.dao.OrderStatusDao;
import com.projectcafeflora.model.Customer;
import com.projectcafeflora.model.Order;
import com.projectcafeflora.model.OrderStatus;
import com.projectcafeflora.service.OrderService;

@SpringBootTest
public class OrderServiceUnitTest {
	
	@Mock
	private OrderDao orDao;
	
	@Mock
	private CustomerDao custDao;
	
	@Mock
	private OrderStatusDao osDao;	
	
	@InjectMocks
	private OrderService oServ;
	
	@InjectMocks
	private OrderService oServ2 = mock(OrderService.class);
	
	Order order, order2;
	Customer custMod;
	OrderStatus osMod;
//	Distribution dMod;
//	Coupon cMod;
	
	List<Order> orderList = new ArrayList<>();
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {}
	
	@BeforeEach
	public void setUp() throws Exception {
		oServ = new OrderService(orDao, custDao, osDao);
		
		custMod = new Customer(1, "johndoe@email.com", "password", "john", "doe", "123 main st.", "new york", "new york", "11111", "5555555555", "1234123412341234", "john doe", 123, 1, 2020);
		
		osMod = new OrderStatus(1, "Pending");
		
		order = new Order(1, custMod, osMod, null, null, 5.99, 0.99, 2.00, 8.98, null, null, null, null);
		
		orderList.add(order);
		
		when(osDao.findByStatusId(1)).thenReturn(osMod);
		when(custDao.findByCustomerId(1)).thenReturn(custMod);
		
		when(orDao.findAll()).thenReturn(orderList);
			
		when(orDao.findByOrderId(Mockito.anyInt())).thenReturn(order);
		
		when(orDao.findByOrderStatus(osMod)).thenReturn(orderList);
		
		when(orDao.findByOrderStatusAndCustomer(osMod, custMod)).thenReturn(orderList);
		
	}
	
	
	@Test
	public void testGetById() {	
		assertEquals(oServ.getById(1), order);				
	}
	
	@Test
	public void testGetByIdFailure() {
		assertNotEquals(oServ.getById(2), null);
	}
	
	@Test
	public void testGetByStatus() {
		assertEquals(oServ.getByStatus(1), orderList);
	}

	@Test
	public void testGetByStatusFailure() {
		assertNotEquals(oServ.getByStatus(1), null);
	}
	
	@Test
	public void testGetAll() {
		assertEquals(oServ.getAll().size(), 1);
	}
	
	@Test
	public void testGetAllFailure() {
		assertNotEquals(oServ.getAll().size(), 2);
	}
	
	@Test
	public void testGetByStatusAndCustomer() {
		assertEquals(oServ.getByStatusAndCustomer(1), orderList);
	}
	
	@Test
	public void testGetByStatusAndCustomerFailure() {
		assertNotEquals(oServ.getByStatusAndCustomer(1), null);
	}
	
	@Test
	public void testDeleteById() {
		oServ2.deleteById(1);
		verify(oServ2, times(1)).deleteById(1);
	}
	
	@Test
	public void testDeleteByIdFailure() {}
	
	@Test
	public void testUpdateStatusDelivering() {
		oServ2.updateStatusDelivering(1);
		verify(oServ2, times(1)).updateStatusDelivering(1);
	}
	
	@Test
	public void testUpdateStatusDeliveringFailure() {}
	
	@Test
	public void testUpdateStatusCancled() {
		oServ2.updateStatusCanceled(1);
		verify(oServ2, times(1)).updateStatusCanceled(1);
	}
	
	@Test
	public void testUpdateStatusCancledFailure() {}
	
	@Test
	public void testUpdateStatusCompleted() {
		oServ2.updateStatusCompleted(1);
		verify(oServ2, times(1)).updateStatusCompleted(1);
	}
	
	@Test
	public void testUpdateStatusCompletedFailure() {}
	
	@Test
	public void testUpdateStatusReady() {
		oServ2.updateStatusReady(1);
		verify(oServ2, times(1)).updateStatusReady(1);
	}
	
	@Test
	public void testUpdateStatusReadyFailure() {}
	
	@Test
	public void testUpdateStatusPreparing() {
		oServ2.updateStatusPreparing(1);
		verify(oServ2, times(1)).updateStatusPreparing(1);
	}
	
	@Test
	public void testUpdateStatusPreparingFailure() {}
	
	@Test
	public void testUpdateOrderTotal() {
		oServ2.updateOrderTotal(1, 1, 1, 1);
		verify(oServ2, times(1)).updateOrderTotal(1, 1, 1, 1);
	}
	
	@Test
	public void testUpdateOrderTotalFailure() {}
	
	@Test
	public void testUpdateStatusOrdered() {
		oServ2.updateStatusOrdered(1, 1, "1", 1, 1, 1, 1);
		verify(oServ2, times(1)).updateStatusOrdered(1, 1, "1", 1, 1, 1, 1);
	}
	
	@Test
	public void testUpdateStatusOrderedFailure() {}
	
	@Test
	public void testInsertNewOrder() {
		oServ2.insertNewOrder(1);
		verify(oServ2, times(1)).insertNewOrder(1);
	}
	
	@Test
	public void testInsertNewOrderFailure() {}
}

















