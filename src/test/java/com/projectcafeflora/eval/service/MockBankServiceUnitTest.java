package com.projectcafeflora.eval.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.projectcafeflora.dao.MockBankDao;
import com.projectcafeflora.model.MockBank;
import com.projectcafeflora.service.MockBankService;

@SpringBootTest
public class MockBankServiceUnitTest {
	
	@Mock
	private MockBankDao mockBankDao;
	
	@InjectMocks
	private MockBankService mockBankServ;
	
	private MockBank mockbank;
	private MockBank mockbank1;
	
	private List<MockBank> MockBankList = new ArrayList<MockBank>();
	
	@BeforeEach 
	private void setUp() throws Exception{
		mockBankServ = new MockBankService(mockBankDao);
		mockbank = new MockBank("1111111111111111", 5000.0, "Daniel Butcher", 999, 1, 2025);
		mockbank1 = new MockBank("3333333333333333", 250.0, "John Smith", 999, 1, 20225);
		MockBankList.add(mockbank);
		MockBankList.add(mockbank1);
		when(mockBankDao.findByCreditCardName("Daniel Butcher")).thenReturn(mockbank);
		when(mockBankDao.findByCreditCardName("John Smith")).thenReturn(mockbank);
		when(mockBankDao.findByCreditCardNumber("1111111111111111")).thenReturn(mockbank);
		when(mockBankDao.findByCreditCardCode(000)).thenReturn(null);
		when(mockBankDao.findByCreditCardNumber("0000000000000000")).thenReturn(null);
		when(mockBankDao.findByCreditCardName("Darth Vader")).thenReturn(null);
		when(mockBankDao.findByCreditCardName("John Doe")).thenReturn(null);
	}
	
	@Test
	public void testFindByCreditCardNumber() {
		assertEquals(mockBankServ.getCreditCardByNumber("1111111111111111"), mockbank);
	}
	
	@Test
	public void testViewByCreditCardNumberFailure() {
		assertEquals(mockBankServ.getCreditCardByNumber("0000000000000000"), null);
	}
	
	@Test
	public void testViewByCreditCardNameSuccess() {
		assertEquals(mockBankServ.getCreditCardByName("Daniel Butcher"), mockbank);
		assertEquals(mockBankServ.getCreditCardByName("John Smith"), mockbank);
	}
	
	@Test
	public void testViewByCreditCardNameFailure() {
		assertEquals(mockBankServ.getCreditCardByName("John Doe"), null);
		assertEquals(mockBankServ.getCreditCardByName("Darth Vader"), null);
	}
	
	// Test for the List
	@Test
	public void getAllAmountByCreditCardFailure() {
		List<MockBank> temp = new ArrayList<MockBank>();
		temp = mockBankServ.getCreditCardByAmount(1000.0);
		// System.out.println(temp);
		boolean check = true;
		if(!temp.isEmpty()) {
			check = false;
		}
		for(int i = 0; i < temp.size(); i++) {
			if(temp.get(i).equals(MockBankList.get(i))){
				// do not anything
			} else {
				check = false;
			}
		}
		assertTrue(check);	
	}
	
	@Test
	public void getAllCodeByCreditCardFailure() {
		List<MockBank> temp = new ArrayList<MockBank>();
		temp = mockBankServ.getCreditCardByCode(123);
		boolean check = true;
		if(!temp.isEmpty()) {
			check = false;
		}
		for(int i = 0; i < temp.size(); i++) {
			if(temp.get(i).equals(MockBankList.get(i))) {
				// do not anything
			} else {
				check = false;
			}
		}
		assertTrue(check);
	}
	
	@Test
	public void getAllAmountByCreditCardSuccess() {
		List<MockBank> temp = new ArrayList<MockBank>();
		temp = mockBankServ.getCreditCardByAmount(5000.0);
		boolean check = true;
		if(!temp.isEmpty()) {
			check = false;
		}
		for(int i = 0; i < temp.size(); i++) {
			if(temp.get(i).equals(MockBankList.get(i))) {
				// do not anything
			} else {
				check = false;
			}
		}
		assertTrue(check);
	}
	
	@Test
	public void getAllCodeByCreditCardSuccess() {
		List<MockBank> temp = new ArrayList<MockBank>();
		temp = mockBankServ.getCreditCardByCode(999);
		boolean check = true;
		if(!temp.isEmpty()) {
			check = false;
		}
		for(int i = 0; i < temp.size(); i++) {
			if(temp.get(i).equals(MockBankList.get(i))) {
				// do not anything
			}else {
				check = false;
			}
		}
		assertTrue(check);
	}
}
