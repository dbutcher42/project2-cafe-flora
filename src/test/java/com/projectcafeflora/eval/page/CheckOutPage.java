package com.projectcafeflora.eval.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckOutPage {
	private WebDriver driver;

	@FindBy(xpath = "//*[@id='pageheader']")
	private WebElement pageHeader;
	
	@FindBy(xpath = "//*[@id='couponCode']")
	private WebElement couponCodeEntry;
	
	@FindBy(xpath = "//*[@id='deliveryOption']")
	private WebElement deliveryButton;
	
	@FindBy(xpath = "//*[@id='pickupOption']")
	private WebElement pickupButton;
	
	@FindBy(xpath = "//*[@id='deliveryTip']")
	private WebElement tipEntry;
	
	@FindBy(xpath = "//*[@id='submitorder']")
	private WebElement proceedPaymentButton;
	
	@FindBy(xpath = "//*[@id='backtocart']")
	private WebElement goToCartButton;
	
	@FindBy(xpath = "//*[@id=\"subtotal\"]")
	private WebElement subtotal;
	
	@FindBy(xpath = "//*[@id='discount']")
	private WebElement couponDiscount;
	
	@FindBy(xpath = "//*[@id='tax']")
	private WebElement tax;
	
	@FindBy(xpath = "//*[@id='tip']")
	private WebElement tip;
	
	@FindBy(xpath = "//*[@id='total']")
	private WebElement total;
	
	@FindBy(xpath = "//*[@id='logout']")
	private WebElement logoutButton;
	
	public CheckOutPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public String getPageHeader() {
		return this.pageHeader.getText();
	}
	
	public void setCoupon(String couponCode) {
		this.couponCodeEntry.clear();
		this.couponCodeEntry.sendKeys(couponCode);
	}

	public void clickDelivery() {
		this.deliveryButton.click();
	}
	
	public void clickPickup() {
		this.pickupButton.click();
	}
	
	public void setTip(String tip) {
		this.tipEntry.clear();
		this.tipEntry.sendKeys(tip);
	}
	
	public void clickProceedPayment() {
		this.proceedPaymentButton.click();
	}
	
	public void clickGoToCart() {
		this.goToCartButton.click();
	}
	
	public void clickLogout() {
		this.logoutButton.click();
	}
	
	public String getSubtotal() {
		return this.subtotal.getText();
	}

	public String getDiscount() {
		return this.couponDiscount.getText();
	}
	
	public String getTax() {
		return this.tax.getText();
	}
	
	public String getTip() {
		return this.tip.getText();
	}
	
	public String getTotal() {
		return this.total.getText();
	}

}
