package com.projectcafeflora.eval.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaymentPage {
	private WebDriver driver;
	
	@FindBy(xpath = "//*[@id='pageheader']")
	private WebElement pageHeader;
	
	@FindBy(xpath = "//*[@id='logout']")
	private WebElement logoutButton;
	
	@FindBy(xpath = "//*[@id='total']")
	private WebElement total;
	
	@FindBy(xpath = "//*[@id='ccname']")
	private WebElement ccName;
	
	@FindBy(xpath = "//*[@id='ccnum']")
	private WebElement ccNumber;
	
	@FindBy(xpath = "//*[@id='ccmonth']")
	private WebElement ccMonth;
	
	@FindBy(xpath = "//*[@id='ccyear']")
	private WebElement ccYear;
	
	@FindBy(xpath = "//*[@id='cccode']")
	private WebElement ccCode;
	
	@FindBy(xpath = "//*[@id='usesaved']")
	private WebElement savedButton;
	
	@FindBy(xpath = "//*[@id='submit']")
	private WebElement submitButton;
	
	@FindBy(xpath = "//*[@id='backtocheckout']")
	private WebElement backToCheckoutButton;
	
	public PaymentPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public String getPageHeader() {
		return this.pageHeader.getText();
	}
	
	public void clickLogout() {
		this.logoutButton.click();
	}

	public String getTotal() {
		return total.getText();
	}

	public String getCcName() {
		return ccName.getAttribute("value");
	}

	public void setCcName(String ccName) {
		this.ccName.clear();
		this.ccName.sendKeys(ccName);
	}

	public String getCcNumber() {
		return ccNumber.getAttribute("value");
	}

	public void setCcNumber(String ccNumber) {
		this.ccNumber.clear();
		this.ccNumber.sendKeys(ccNumber);
	}

	public String getCcMonth() {
		return ccMonth.getAttribute("value");
	}

	public void setCcMonth(String ccMonth) {
		this.ccMonth.clear();
		this.ccMonth.sendKeys(ccMonth);
	}

	public String getCcYear() {
		return ccYear.getAttribute("value");
	}

	public void setCcYear(String ccYear) {
		this.ccYear.clear();
		this.ccYear.sendKeys(ccYear);
	}

	public String getCcCode() {
		return ccCode.getAttribute("value");
	}

	public void setCcCode(String ccCode) {
		this.ccCode.clear();
		this.ccCode.sendKeys(ccCode);
	}
	
	public void clickUseSaved() {
		this.savedButton.click();
	}
	
	public void clickSubmit() {
		this.submitButton.click();
	}
	
	public void clickBacktoCheckout() {
		this.backToCheckoutButton.click();
	}
	
}
