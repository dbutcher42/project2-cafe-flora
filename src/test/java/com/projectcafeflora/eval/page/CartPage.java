package com.projectcafeflora.eval.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CartPage {
	
	private WebDriver driver;
	
	@FindBy(xpath = "//*[@id='checkout-btn']")
	private WebElement goToCheckoutButton;
	
	@FindBy(xpath = "//*[@id='cart-name']")
	private WebElement pageHeader;
	
	@FindBy(xpath = "//*[@id='cart-total']")
	private WebElement cartTotal;
	
	@FindBy(xpath = "//*[@id='clear-cart-btn']")
	private WebElement clearCartBtn;
	
	@FindBy(xpath = "//*[@id='removeItem1']")
	private WebElement removeItemBtn;
	
	@FindBy(xpath = "//*[@id='addItem0']")
	private WebElement addItemBtn;

	@FindBy(xpath = "//*[@id='logout']")
	private WebElement logoutButton;

	
	public CartPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickClearCartBtn() {
		this.clearCartBtn.click();
	}
	
	public void clickRemoveItemBtn() {
		this.removeItemBtn.click();
	}
	
	public void clickAddItemBtn() {
		this.addItemBtn.click();
	}
	
	public void clickCheckoutButton() {
		this.goToCheckoutButton.click();
	}
	
	public String getPageHeader() {
		return this.pageHeader.getText();
	}
	
	public String getCartTotal() {
		return this.cartTotal.getText();
	}

	public void clickLogout() {
		this.logoutButton.click();
	}

}
