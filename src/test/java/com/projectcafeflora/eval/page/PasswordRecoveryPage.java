package com.projectcafeflora.eval.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PasswordRecoveryPage {
	
	private WebDriver driver;
	
	@FindBy(xpath = "//*[@id='pageheader']")
	private WebElement pageHeader;
	
	@FindBy(xpath = "//*[@id='email']")
	private WebElement email;
	
	@FindBy(xpath = "//*[@id='firstName']")
	private WebElement firstName;
	
	@FindBy(xpath = "//*[@id='lastName']")
	private WebElement lastName;
	
	@FindBy(xpath = "//*[@id='submit']")
	private WebElement submitButton;
	
	@FindBy(xpath = "//*[@id='backtologin']")
	private WebElement loginButton;
	
	@FindBy(xpath = "//*[@id='message']")
	private WebElement message;
	
	public PasswordRecoveryPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void setEmail(String email) {
		this.email.clear();
		this.email.sendKeys(email);
	}

	public void setFirstName(String firstName) {
		this.firstName.clear();
		this.firstName.sendKeys(firstName);
	}
	
	public void setLastName(String lastName) {
		this.lastName.clear();
		this.lastName.sendKeys(lastName);
	}
	
	public String getPageHeader() {
		return this.pageHeader.getText();
	}
	
	public String getMessage() {
		return this.message.getText();
	}
	
	public void backToLogin() {
		this.loginButton.click();
	}
	
	public void submit() {
		this.submitButton.click();
	}
}
