package com.projectcafeflora.eval.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class VerificationPage {
	
	private WebDriver driver;
	
	@FindBy(xpath = "//*[@id='pageheader']")
	private WebElement pageHeader;
	
	@FindBy(xpath = "//*[@id='customer']")
	private WebElement customer;
	
	@FindBy(xpath = "//*[@id='distribution']")
	private WebElement distribution;
	
	@FindBy(xpath = "//*[@id='message1']")
	private WebElement message1;
	
	@FindBy(xpath = "//*[@id='message2']")
	private WebElement message2;
	
	@FindBy(xpath = "//*[@id='submit']")
	private WebElement submitButton;
	
	public VerificationPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public String getPageHeader() {
		return pageHeader.getText();
	}

	public String getCustomer() {
		return customer.getText();
	}

	public String getDistribution() {
		return distribution.getText();
	}

	public String getMessage1() {
		return message1.getText();
	}

	public String getMessage2() {
		return message2.getText();
	}
	
	public void clickSubmit() {
		this.submitButton.click();
	}

}
