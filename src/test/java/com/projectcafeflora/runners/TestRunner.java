package com.projectcafeflora.runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
//		features= {"features/Verification.feature", "features/Menu.feature", "features/CheckOut.feature", "features/Login.feature", 
//				"features/CheckOut.feature", "features/Payment.feature", "features/Verification.feature", "features/PasswordRecovery.feature",
//				"features/Registration.feature", "features/Home_Navbar.feature", "features/Cart.feature",
//		},
//		features= {"features/Verification.feature"},
//		features= {"features/Menu.feature"},
//		features= {"features/CheckOut.feature"},
//		features= {"features/Login.feature"},
//		features= {"features/Payment.feature"},
//		features= {"features/Cart.feature"},
//		features= {"features/PasswordRecovery.feature"},
//		features= {"features/Registration.feature"},
		features= {"features/Home_Navbar.feature"},
		glue = {"com.projectcafeflora.gluecode"}
		)
public class TestRunner {

}
