Feature: Cafe Flora Menu
#	#As a User, I wish to see the different items on the Menu

	Scenario Outline: Seeing an item on the Menu
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user finds an item Coffee
		Then the item is visible
	
	Examples:
		| email           | password |
		| email@email.com | password |
	
 	#As a User, I wish to select items from the Menu, the user will need to be logged in
	
	Scenario Outline: Selecting item from Menu
		Given a user is at the login page of Cafe Flora second time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user clicks add to cart for Burrito
		Then a message will appear
		
	Examples:
		| email           | password |
		| email@email.com | password |
		 
 	#As a User, I want the items I select to be added to my Cart
	Scenario Outline: Selecting items from a Menu and going to Cart
		Given a user is at the login page of Cafe Flora third time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user clicks add to cart for Bacon
		And a user clicks add to cart for Coffee
		And a user clicks add to cart for Waffles
		And the user clicks on go to cart
		Then the user is redirected to the cart
		
	Examples:
		| email           | password |
		| email@email.com | password |
	
 	#As a User, I wish to select the same item multiple times
	Scenario Outline: Select an item multiple times and go to cart
		Given a user is at the login page of Cafe Flora fourth time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user clicks add to cart for Burrito three times
		And the user clicks on go to cart
		Then the user is redirected to the cart
		
	Examples:
		| email           | password |
		| email@email.com | password |