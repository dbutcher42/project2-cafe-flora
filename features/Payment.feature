Feature: Cafe Flora Payment
	As a user I wish pay for my order
	
	Scenario Outline: Paying for order without saved information
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user clicks the add burrito button twice
		And then a user clicks go to cart
		And then a user clicks checkout
		And a user inputs their coupon code "<couponcode>"
		And a user enters a tip "<tip>"
		And then a user submits to payment
		And a user inputs their name "<name>"
		And a user inputs their cc number "<ccnum>"
		And a user inputs their cc month "<ccmonth>"
		And a user inputs their cc year "<ccyear>"
		And a user inputs their cc code "<cccode>"
		And then a user clicks process payment
		Then a user is redirected to the verification page  
	
	Examples:
		| email           | password | couponcode     | tip | name           | ccnum            | ccmonth | ccyear | cccode |
		| email@email.com | password | halfoffburrito |	2   | Daniel Butcher | 1111111111111111 | 1       | 2025   | 999    |

	Scenario Outline: Paying for order with saved information
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user clicks the add burrito button twice
		And then a user clicks go to cart
		And then a user clicks checkout
		And a user inputs their coupon code "<couponcode>"
		And a user enters a tip "<tip>"
		And then a user submits to payment
		And then a user clicks on use saved CC info
		And then a user clicks process payment
		Then a user is redirected to the verification page  
	
	Examples:
		| email           | password | couponcode     | tip |
		| email@email.com | password | halfoffburrito |	2   |