Feature: Cafe Flora Home
	As a user I wish to use the navbar features of the home page
	
	Scenario Outline: logging into the Cafe Flora and opening for the link and click the new home page
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		Then the user is redirected to home page
		
	Examples:
		| email           | password |
		| email@email.com | password |
	
	Scenario Outline: logging into the Cafe Flora and opening the link and click the new menu page
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		Then a user is redirected to the menu page
	
	Examples:
		| email           | password |
		| email@email.com | password |
		
	Scenario Outline: logging into the Cafe Flora and opening the link and click the new cart page
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		Then the user is redirected to home page
	
	Examples:
		| email           | password |
		| email@email.com | password |
	
	
#	Scenario Outline: Opening the footer and click the new project page
#		Given a user clicks the project button first time
#		When a user inputs their email "<email>"
#		And a user inputs their password "<password>"
#		And then the user submits the information
#		And a user clicks the project button
#		Then a user is redirected to the new page
#	
#	Examples:
#		| email           | password |
#		| email@email.com | password |
#	
#	
#	Scenario Outline: Opening the footer and click the new about us page
#		Given a user clicks the about us button first time
#		When a user inputs their email "<email>"
#		And a user inputs their password "<password>"
#		And then the user submits the information
#		And a user clicks the about us button
#		Then a user is redirected to the new page
#	
#	Examples:
#		| email           | password |
#		| email@email.com | password |
#	