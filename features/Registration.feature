Feature: Cafe Flora Registration
	As a user is trying to create a new account or going back to the login page from registration page
	
#	Scenario Outline: Logging into Cafe Flora
#		Given a user is at the login page to redirect to register page
#		When a click the register a account link
#		Then the user gets redirected to the registration page
#	
#	
#	Examples:
#		|	|
#		|	|
#	
	Scenario Outline: Go to the registration page
		Given a user is at the login page of Cafe Flora second time
		When a user clicks the Create a New Account link
		When a user inputs their first name "<firstName>"
		And a user inputs their last name "<lastName>"
		And a user inputs a new email "<email>"
		And a user inputs a new password "<password>"
		And a user inputs their address "<address>"
		And a user inputs their city "<city>"
		And a user inputs their state "<state>"
		And a user inputs their zipCode "<zipCode>"
		And a user inputs their phonenumber "<phone>"
		And a user inputs their credit card name "<creditCardName>"
		And a user inputs their credit card number "<creditCardNumber>"
		And a user inputs their credit card month "<creditCardMonth>"
		And a user inputs their credit card year "<creditCardYear>"
		And a user inputs their credit card code "<creditCardCode>"
		And then the user sumbit the form
		Then the user gets redirected to the login page again

	Examples:
		| firstName		| lastName		| email						| password		| address			| city			| state			| zipCode	|phone		| creditCardName	| creditCardNumber	| creditCardMonth	| creditCardYear	| creditCardCode	| 
#		| Cafeflora 	| Manager 		| CafeFloraEmail@gmail.com	| Cafeflora!	| 1234 Sample Ave	| San Fransisco	| California	| 12345		|1234567890	| CafeFlora Manager	| 1234123467896789	| 1					| 2021 				| 111				|
		| Cafeflora5 	| Manager4 		| Dantest2@gmail.com		| Cafeflora!	| 1234 Sample Ave	| San Fransisco	| California	| 12346		|1234567892 | CafeFlora Manage3	| 1234123467896782	| 2					| 2022 				| 112				|
		
	Scenario Outline: Go back to login page 
		Given a user is at the login page of Cafe Flora second time
		When a user clicks the Create a New Account link
		And a user clicked the login link at the buttom of the page
		Then the user gets redirected to the login page again
		
	Examples:
		|	|	
		|	|
	
#	Scenario Outline: Goto the recover password page
#		Given a user is at the login page of Cafe Flora third time
#		When a user clicks the Forget Password link
#		And a user inputs a valid email "<email>"
#		And a user inputs a valid first name "<firstName>"
#		And a user inputs a valid last name "<lastName>"
#		Then the user is redirected to the recover password page
#
#	Examples:
#		| email						| firstName 	| lastName		| 
#		| CafeFloraTest@gmail.com	| CafeFlora 	| Test 			|